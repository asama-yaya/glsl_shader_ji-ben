#version 330
// shadertype=<glsl>
// frag

// テクスチャ
uniform sampler2D tex1;

// もらう変数
in vec4 position;
in vec3 normal;
in vec4 texcoord;

// フレームバッファに出力するデータ
layout (location = 0) out vec4 fc1; 

#if 1
void main(void){

	// テクスチャ
	vec4 color1 = texture2DProj(tex1, texcoord);

	// light
	vec3 lightPosition = vec3(100.0, 100.0, 500.0);
	vec3 light = normalize(lightPosition - position.xyz);

	// diffuse
	vec3 fnormal = normalize(normal);
	float diffuse = max(dot(light, fnormal), 0.0);
  
	// specular
	vec3 view = -normalize(position.xyz);
	vec3 halfway = normalize(light + view);
	float specular = pow(max(dot(fnormal, halfway), 0.0), 20.0);

	// 出力
	fc1 = color1 * diffuse + vec4(1.0, 1.0, 1.0, 1.0) * specular;
	//fc1 = color1;

	// ちゃんと光源のマテリアルとか気にするならこんな感じ
	// マテリアルの設定とかは事前にCPUからもらっておくとよい
	//fc1 = color * gl_LightSource[0].diffuse * diffuse
	//			+ gl_FrontLightProduct[0].specular * specular
	//			+ color * gl_LightSource[0].ambient;
}
#endif

#if 0
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 鏡面反射のモデル
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// 光源
vec3 La = vec3(0.0, 0.0, 0.0);//gl_LightSource[0].ambient.xyz;    // ライト環境光
vec3 Ld = vec3(0.5, 0.5, 0.5);//gl_LightSource[0].diffuse.xyz;    // ライト拡散反射光
vec3 Ls = vec3(1.0, 1.0, 1.0);//gl_LightSource[0].specular.xyz;    // ライト鏡面反射光
vec3 Lp = vec3(100.0, 100.0, 500.0);//gl_LightSource[0].position.xyz;    // ライト位置
 
// 材質
vec3 Ke = vec3(0.0, 0.0, 0.0);//gl_FrontMaterial.emission.xyz;    // 放射色
vec3 Ka = vec3(0.3, 0.3, 0.3);//gl_FrontMaterial.ambient.xyz;        // 環境光
vec3 Kd = vec3(1.0, 1.0, 1.0);//gl_FrontMaterial.diffuse.xyz;        // 拡散反射
vec3 Ks = vec3(1.0, 1.0, 1.0);//gl_FrontMaterial.specular.xyz;    // 鏡面反射
float shine = 20.0;//gl_FrontMaterial.shininess;

void main(void)
{
	vec4 color1 = texture2DProj(tex1, texcoord);

	float m = 0.10f;
	float refrac = 20.0f;
 
    vec3 V = normalize(-position.xyz);        // 視線ベクトル
    vec3 N = normalize(normal);            // 法線ベクトル
    vec3 L = normalize(Lp-position.xyz);    // ライトベクトル
    vec3 H = normalize(L+V);            // ハーフベクトル
 
    // 放射色の計算
    vec3 emissive = Ke;
 
    // 環境光の計算
    vec3 ambient = La;
 
    // 拡散反射の計算
    float diffuseLight = max(dot(L, N), 0.0);
    vec3 diffuse = vec3(diffuseLight,diffuseLight,diffuseLight);
 
    // 鏡面反射の計算
    vec3 specular = vec3(0.0);
    if(diffuseLight > 0.0){
        // Cook-Torrance反射モデルの計算
        float NH = dot(N, H);
        float VH = dot(V, H);
        float NV = dot(N, V);
        float NL = dot(N, L);
 
        float alpha = acos(NH);
 
        // D:ベックマン分布関数 to blin torrans sppawroe
        float D = (1.0/(4*m*m*NH*NH*NH*NH))*exp((NH*NH-1)/(m*m*NH*NH));
		//float D = exp(-(acos(NH) * 2.0f)*(acos(NH) * 2.0f));
 
        // G:幾何減衰
        float G = min(1, min((2*NH*NV)/VH, (2*NH*NL)/VH));
 
        // F:フレネル項
        float c = VH;
        float g = sqrt(refrac*refrac+c*c-1);
        float F = ((g-c)*(g-c)/((g+c)*(g+c)))*(1+(c*(g+c)-1)*(c*(g+c)-1)/((c*(g-c)-1)*(c*(g-c)-1)));
 

        float specularLight = D*G*F/NV;
		//float specularLight = pow(max(dot(H, N), 0.0), 90.0f);

        specular = Ks*Ls*specularLight;
    }
	vec3 aaa = vec3(1.0f, 1.0f, 0.0f);
    fc1.xyz = aaa * (emissive+ambient+diffuse).x  + specular;
    fc1.w = 1.0;
}
#endif
